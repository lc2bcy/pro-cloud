package com.cloud.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.admin.beans.po.SysRoleMenu;

/**
 * 角色-菜单
 *
 * @author Aijm
 * @date 2019-08-25 21:12:49
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
