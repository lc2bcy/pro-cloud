package com.cloud.admin.util;


import cn.hutool.core.bean.BeanUtil;
import com.cloud.admin.beans.dto.OfficeDTO;
import com.cloud.admin.beans.dto.RoleDTO;
import com.cloud.admin.beans.dto.UserDTO;
import com.cloud.admin.beans.po.SysMenu;
import com.cloud.admin.beans.po.SysOffice;
import com.cloud.admin.beans.po.SysUser;
import com.cloud.admin.service.SysMenuService;
import com.cloud.admin.service.SysOfficeService;
import com.cloud.admin.service.SysRoleService;
import com.cloud.admin.service.SysUserService;
import com.cloud.common.data.util.SpringUtil;
import com.cloud.common.security.component.SecurityUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;

/**
 *  用户工具类  不使用service的原因是 防止初夏
 * @author Aijm
 * @since 2019/8/25
 */
public class UserUtil {


    private static SysMenuService sysMenuService = SpringUtil.getBean(SysMenuService.class);

    private static SysRoleService sysRoleService = SpringUtil.getBean(SysRoleService.class);

    private static SysUserService sysUserService = SpringUtil.getBean(SysUserService.class);

    private static SysOfficeService sysOfficeService = SpringUtil.getBean(SysOfficeService.class);

    /**
     * 判断该用户是不是超级管理员 并给admin赋值
     * @param id 用户id
     * @return
     */
    public static boolean hasAdmin(Long id){
        return id != null && (id== 1);
    }


    /**
     * 获取登录用户的信息
     * @return
     */
    public static Long getUserId(){
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        SecurityUser user = (SecurityUser)authentication.getPrincipal();
        return user.getUserId();
    }


    /**
     * 获取登录用户的信息 登录名
     * @return
     */
    public static String getUserName(){
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        SecurityUser user = (SecurityUser)authentication.getPrincipal();
        return user.getUsername();
    }

    /**
     * 获取当前用户授权菜单
     * @return
     */
    public static List<SysMenu> getMenuList(){
        Long userId = getUserId();
        List<SysMenu> menuList = sysMenuService.findByUserId(userId);
        return menuList;
    }

    /**
     * 获取当前用户详细信息 不包括角色 和部门信息
     * @return
     */
    public static SysUser getUser(){
        Long userId = getUserId();
        return sysUserService.getById(userId);
    }

    /**
     * 获取用户详细 信息 包含 部门信息和角色信息
     * @return
     */
    public static UserDTO getUserDTO(){
        UserDTO userDTO = new UserDTO();
        // 获取用户信息
        SysUser sysUser = getUser();
        BeanUtil.copyProperties(sysUser, userDTO);
        // 获取用户部门信息
        SysOffice userOffice = getUserOffice();
        OfficeDTO officeDTO = new OfficeDTO();
        BeanUtil.copyProperties(userOffice, officeDTO);
        userDTO.setOffice(officeDTO);
        // 获取角色信息
        userDTO.setRoleList(getRoleList());
        return userDTO;
    }



    /**
     * 获取到当前用户 部门信息
     * @return
     */
    public static SysOffice getUserOffice() {
        SysUser user = getUser();
        return sysOfficeService.getById(user.getOfficeId());
    }

    /**
     * 获取当前用户的角色信息
     * @return
     */
    public static List<RoleDTO> getRoleList(){
        return  sysRoleService.findList(getUser());
    }

}
