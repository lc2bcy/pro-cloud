package com.cloud.admin.service.impl;

import com.cloud.common.data.base.BaseService;
import com.cloud.admin.beans.dto.RoleDTO;
import com.cloud.admin.beans.po.SysRole;
import com.cloud.admin.beans.po.SysUser;
import com.cloud.admin.mapper.SysRoleMapper;
import com.cloud.admin.service.SysRoleService;
import com.cloud.common.cache.annotation.Cache;
import com.cloud.common.cache.annotation.CacheClear;
import com.cloud.common.cache.annotation.CacheConf;
import com.cloud.common.cache.constants.CacheScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * 角色表
 *
 * @author Aijm
 * @date 2019-08-25 20:57:31
 */
@Service
public class SysRoleServiceImpl extends BaseService<SysRoleMapper, SysRole> implements SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    @CacheClear(scope = CacheScope.user_role, key = "'*'", pattern = true)
    public boolean updateById(SysRole entity) {
        return super.updateById(entity);
    }

    @Override
    @CacheClear(scope = CacheScope.user_role, key = "'*'", pattern = true)
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @Override
    @Cache(scope = CacheScope.user_role, key = "#sysUser.id")
    public List<RoleDTO> findList(SysUser sysUser) {
        return sysRoleMapper.findList(sysUser);
    }
}
