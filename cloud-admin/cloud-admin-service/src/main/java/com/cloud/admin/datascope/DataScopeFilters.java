package com.cloud.admin.datascope;

import cn.hutool.core.util.StrUtil;
import com.cloud.admin.beans.dto.RoleDTO;
import com.cloud.admin.beans.dto.UserDTO;
import com.cloud.admin.beans.po.SysOffice;
import com.cloud.admin.beans.po.SysRole;
import com.cloud.admin.util.UserUtil;
import com.cloud.common.entity.BaseEntity;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @Author Aijm
 * @Description 对数据权限的实现
 * @Date 2019/5/8
 */
@Slf4j
public class DataScopeFilters {


    /**
     * 数据范围过滤 默认关联create_by
     * @param officeAlias 机构表别名
     * @param masterAlias 主表别名
     * @return 标准连接条件对象
     */
    public static String dataScopeFilter(String officeAlias, String masterAlias) {
        // 获取到登录的用户信息

        StringBuilder sqlString = new StringBuilder();

        // 获取到最大的数据权限范围
        int dataScopeInteger = 4;
        for (RoleDTO r : UserUtil.getRoleList()){
            int ds = Integer.valueOf(r.getDataScope());
            if (ds < dataScopeInteger){
                dataScopeInteger = ds;
            }
        }
        // 超级管理员，跳过权限过滤
        Long userId = UserUtil.getUserId();
        SysOffice userOffice = UserUtil.getUserOffice();
        if (!UserUtil.hasAdmin(userId)){
            boolean isDataScopeAll = false;

            if (DataScope.DATA_SCOPE_ALL.equals(dataScopeInteger)){
                isDataScopeAll = true;
            } else if (DataScope.DATA_SCOPE_OFFICE_AND_CHILD.equals(dataScopeInteger)){
                sqlString.append(officeAlias + ".id = '" + userOffice.getId() + "'");
                sqlString.append(" OR " + officeAlias + ".parent_ids LIKE '" + userOffice.getParentIds() + userOffice.getId() + ",%'");
            } else if (DataScope.DATA_SCOPE_OFFICE.equals(dataScopeInteger)){
                sqlString.append(officeAlias + ".id = '" + userOffice.getId() + "'");
            } else if (DataScope.DATA_SCOPE_SELF.equals(dataScopeInteger)) {
                sqlString.append(masterAlias + ".create_by = '" + userId + "'");
            }

            // 如果没有全部数据权限，并设置了用户别名，则当前权限为本人；如果未设置别名，当前无权限为已植入权限
            if (!isDataScopeAll){
                if (StrUtil.isNotBlank(sqlString.toString())){
                    return " AND (" + sqlString.toString() + ")";
                }
            }

        }
        return "";
    }

    /**
     * 数据范围过滤
     * @param officeAlias 机构表别名
     * @param alias 用户id字段表别名
     * @param field 用户id字段
     * @return 标准连接条件对象
     */
    public static String dataScopeFilter(String officeAlias, String alias, String field) {
        // 获取到登录的用户信息

        StringBuilder sqlString = new StringBuilder();

        // 获取到最大的数据权限范围
        int dataScopeInteger = 4;
        for (RoleDTO r : UserUtil.getRoleList()){
            int ds = Integer.valueOf(r.getDataScope());
            if (ds < dataScopeInteger){
                dataScopeInteger = ds;
            }
        }
        // 超级管理员，跳过权限过滤
        Long userId = UserUtil.getUserId();
        SysOffice userOffice = UserUtil.getUserOffice();
        if (!UserUtil.hasAdmin(userId)){
            boolean isDataScopeAll = false;

            if (DataScope.DATA_SCOPE_ALL.equals(dataScopeInteger)){
                isDataScopeAll = true;
            } else if (DataScope.DATA_SCOPE_OFFICE_AND_CHILD.equals(dataScopeInteger)){
                sqlString.append(officeAlias + ".id = '" + userOffice.getId() + "'");
                sqlString.append(" OR " + officeAlias + ".parent_ids LIKE '" + userOffice.getParentIds() + userOffice.getId() + ",%'");
            } else if (DataScope.DATA_SCOPE_OFFICE.equals(dataScopeInteger)){
                sqlString.append(officeAlias + ".id = '" + userOffice.getId() + "'");
            } else if (DataScope.DATA_SCOPE_SELF.equals(dataScopeInteger)) {
                sqlString.append(alias + "."+field+"= '" + userId + "'");
            }

            // 如果没有全部数据权限，并设置了用户别名，则当前权限为本人；如果未设置别名，当前无权限为已植入权限
            if (!isDataScopeAll){
                if (StrUtil.isNotBlank(sqlString.toString())){
                    return " AND (" + sqlString.toString() + ")";
                }
            }

        }
        return "";
    }


}
