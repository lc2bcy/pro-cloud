package com.cloud.admin.controller;

import cn.hutool.core.util.StrUtil;
import com.cloud.admin.beans.dto.MenuDTO;
import com.cloud.admin.beans.po.SysMenu;
import com.cloud.admin.beans.vo.MenuTreeVO;
import com.cloud.admin.beans.vo.OfficeTreeVO;
import com.cloud.admin.beans.vo.OfficeVO;
import com.cloud.admin.util.UserUtil;
import com.cloud.common.data.util.ObjUtil;
import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.admin.beans.po.SysOffice;
import com.cloud.admin.service.SysOfficeService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;


/**
 * 机构表
 *
 * @author Aijm
 * @date 2019-08-25 20:45:42
 */
@RestController
@RequestMapping("/sysoffice" )
@Api(value = "sysoffice", tags = "sysoffice管理")
public class SysOfficeController {

    @Autowired
    private SysOfficeService sysOfficeService;

    /**
     * 查询 所有的部门
     * @return
     */
    @GetMapping("/listALL")
    @PreAuthorize("@pms.hasPermission('admin_sysoffice_view')")
    public Result getSysOfficeAll() {
        List<SysOffice> offices = sysOfficeService.list(Wrappers.emptyWrapper());
        List<SysOffice> officeList = Lists.newArrayList();

        ObjUtil.sortList(officeList, offices, ObjUtil.getRootId(), true);
        return Result.success(OfficeVO.builder().officeList(officeList).build());
    }


    /**
     * 通过id查询机构表
     * @param id id
     * @return Result
     */
    @GetMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('admin_sysoffice_view')")
    public Result getById(@PathVariable("id") Long id) {
        return Result.success(sysOfficeService.getById(id));
    }

    /**
     * 新增机构表
     * @param sysOffice 机构表
     * @return Result
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('admin_sysoffice_add')")
    public Result save(@RequestBody @Valid SysOffice sysOffice) {
        return Result.success(sysOfficeService.save(sysOffice));
    }

    /**
     * 修改机构表
     * @param sysOffice 机构表
     * @return Result
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('admin_sysoffice_edit')")
    public Result updateById(@RequestBody @Valid SysOffice sysOffice) {
        return Result.success(sysOfficeService.updateById(sysOffice));
    }

    /**
     * 通过id删除机构表
     * @param id id
     * @return Result
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('admin_sysoffice_del')")
    public Result removeById(@PathVariable Long id) {
        return Result.success(sysOfficeService.removeById(id));
    }


    /**
     *  弹出树
     * @param extId 表示当前节点的id
     * @return
     */
    @PreAuthorize("@pms.hasPermission('admin_sysoffice_view')")
    @GetMapping(value = "treeData")
    public Result treeData(@RequestParam(required=false) String extId) {
        List<Map<String, Object>> mapList = Lists.newArrayList();
        List<SysOffice> list = sysOfficeService.list(Wrappers.emptyWrapper());

        for (SysOffice sysOffice : list) {
            boolean hasExtId = extId != null && !extId.equals(sysOffice.getId()) && sysOffice.getParentIds().indexOf("," + extId + ",") == -1;
            if (StrUtil.isBlank(extId) || hasExtId ){

                Map<String, Object> map = Maps.newHashMap();
                map.put("id", sysOffice.getId());
                map.put("pId", sysOffice.getParentId());
                map.put("name", sysOffice.getName());
                mapList.add(map);
            }
        }
        return Result.success(OfficeTreeVO.builder().tree(mapList).build());
    }
}
