package com.cloud.admin.beans.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cloud.common.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 角色表
 *
 * @author Aijm
 * @date 2019-08-25 20:57:31
 */
@Data
@TableName("sys_role")
@Accessors(chain = true)
@ApiModel(description = "角色表")
public class SysRole extends BaseEntity {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "角色名称")
    private String name;

    @ApiModelProperty(value = "英文名称")
    private String enname;

    @ApiModelProperty(value = "角色类型")
    private String roleType;

    @ApiModelProperty(value = "数据范围")
    private String dataScope;

    @ApiModelProperty(value = "是否系统数据")
    @TableField("is_sys")
    private String hasSys;

    @ApiModelProperty(value = "是否可用")
    private String useable;


}
