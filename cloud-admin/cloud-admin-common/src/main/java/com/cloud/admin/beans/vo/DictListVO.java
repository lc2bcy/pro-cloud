package com.cloud.admin.beans.vo;

import com.cloud.admin.beans.po.SysDictList;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author Aijm
 * @Description 字典list 返回
 * @Date 2019/9/6
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DictListVO implements Serializable {

    private static final long serialVersionUID=1L;

    private List<SysDictList> dictLists;
}
