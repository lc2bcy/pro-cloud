package com.cloud.admin.beans.qo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;


/**
 *  登录前台传递参数
 * @author Aijm
 * @since 2019/5/16
 */
@Data
@ToString
public class LoginRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    String username;
    @JsonIgnore
    String password;
    String verifycode;

}
