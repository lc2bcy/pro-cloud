package com.cloud.admin.beans.vo;

import com.cloud.admin.beans.po.SysMenu;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 菜单集合
 * @author Aijm
 * @since 2019/9/4
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MenuVO implements Serializable {
    private static final long serialVersionUID=1L;

    private List<SysMenu> menuList;
}
