package com.cloud.exam.beans.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cloud.common.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import java.time.LocalDateTime;

/**
 * 试卷详细信息
 *
 * @author Aijm
 * @date 2019-10-13 23:30:51
 */
@Data
@TableName("exam_paper")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "试卷详细信息")
public class ExamPaper extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试卷名称")
    private String name;

    @ApiModelProperty(value = "分类ID集合(逗号分隔)")
    private String categoryIds;

    @ApiModelProperty(value = "分类ID")
    private Long categoryId;

    @ApiModelProperty(value = "试卷类型( 1固定试卷  2临时试卷 3班级试卷 4.时段试卷 5.推送试卷)")
    private Integer paperType;

    @ApiModelProperty(value = "级别")
    private Integer gradeLevel;

    @ApiModelProperty(value = "试卷总分(千分制)")
    private Integer paperScore;

    @ApiModelProperty(value = "题目数量")
    private Integer questionCount;

    @ApiModelProperty(value = "建议时长(分钟)")
    private Integer suggestTime;

    @ApiModelProperty(value = "时段试卷 开始时间")
    private LocalDateTime limitStartTime;

    @ApiModelProperty(value = "时段试卷 结束时间")
    private LocalDateTime limitEndTime;

    @ApiModelProperty(value = "试卷框架 内容为JSON")
    private String textContent;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "是否免费：1免费，0收费")
    @TableField("is_free")
    private Integer hasFree;

    @ApiModelProperty(value = "原价")
    private BigDecimal courseOriginal;

    @ApiModelProperty(value = "优惠价")
    private BigDecimal courseDiscount;

    @ApiModelProperty(value = "推荐（0：推荐，1：普通）审核后得到")
    private Integer recommend;

    @ApiModelProperty(value = "状态0:待发布 1：发布上架 2:下架")
    private Integer statusId;

    @ApiModelProperty(value = "规划上架时间")
    private LocalDateTime putawayBegDate;

    @ApiModelProperty(value = "下架时间")
    private LocalDateTime putawayEndDate;

    @ApiModelProperty(value = "审核状态(0:待审核,1:审核通过(通过后自动上架),2:审核不通过)")
    private Integer auditStatus;

    @ApiModelProperty(value = "审核意见")
    private String auditOpinion;

    @ApiModelProperty(value = "试卷评分")
    private Integer score;

    @ApiModelProperty(value = "购买人数")
    private Integer countBuy;

    @ApiModelProperty(value = "学习人数")
    private Integer countStudy;


}
