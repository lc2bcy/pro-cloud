package com.cloud.exam.beans.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cloud.common.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 测试结果详细信息
 *
 * @author Aijm
 * @date 2019-10-14 23:32:31
 */
@Data
@TableName("exam_user_answer")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "测试结果详细信息")
public class ExamUserAnswer extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "题目Id")
    private Long questionId;

    @ApiModelProperty(value = "答案Id")
    private Long examAnswerId;

    @ApiModelProperty(value = "题型")
    private Integer questionType;

    @ApiModelProperty(value = "题目原始分数")
    private Integer questionScore;

    @ApiModelProperty(value = "得分")
    private Integer customerScore;

    @ApiModelProperty(value = "做题答案")
    private String answer;

    @ApiModelProperty(value = "做题内容")
    private String textContent;

    @ApiModelProperty(value = "是否正确 0 错误 1正确")
    private Integer doRight;


}
