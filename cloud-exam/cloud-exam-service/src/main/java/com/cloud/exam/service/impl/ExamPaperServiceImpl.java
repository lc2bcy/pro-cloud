package com.cloud.exam.service.impl;

import com.cloud.common.data.base.BaseService;
import com.cloud.exam.beans.po.ExamPaper;
import com.cloud.exam.mapper.ExamPaperMapper;
import com.cloud.exam.service.ExamPaperService;
import org.springframework.stereotype.Service;

/**
 * 试卷详细信息
 *
 * @author Aijm
 * @date 2019-10-13 23:30:51
 */
@Service
public class ExamPaperServiceImpl extends BaseService<ExamPaperMapper, ExamPaper> implements ExamPaperService {

}
