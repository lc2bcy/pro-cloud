package com.cloud.exam.mapper;

import com.cloud.common.data.base.BaseMapper;
import com.cloud.exam.beans.po.ExamUserAnswer;

/**
 * 测试结果详细信息
 *
 * @author Aijm
 * @date 2019-10-14 23:32:31
 */
public interface ExamUserAnswerMapper extends BaseMapper<ExamUserAnswer> {

}
