package com.cloud.common.oauth.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @Author Aijm
 * @Description userDetailsService 接口再次封装
 * @Date 2019/5/20
 */
public interface ProUserDetailsService extends UserDetailsService {

    /**
     * 根据手机号查询
     * @param mobile
     * @return
     * @throws UsernameNotFoundException
     */
    UserDetails loadUserByMoblie(String mobile) throws UsernameNotFoundException;
}
