package com.cloud.common.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * tree的基础
 * @author Aijm
 * @since 2019/5/4
 */
@Data
@Accessors(chain = true)
public class TreeEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;


	@ApiModelProperty(value = "父级编号")
    protected Long parentId;

    @ApiModelProperty(value = "所有父级编号")
    protected String parentIds;

    @ApiModelProperty(value = "名称")
    protected String name;

    @ApiModelProperty(value = "排序")
    protected Integer sort;

}
