package com.cloud.common.data.base;

import com.cloud.common.entity.BaseEntity;

/**
 * @Author Aijm
 * @Description 接口baseMapper
 * @Date 2019/10/12
 */
public interface BaseMapper<T extends BaseEntity> extends com.baomidou.mybatisplus.core.mapper.BaseMapper<T> {

}
