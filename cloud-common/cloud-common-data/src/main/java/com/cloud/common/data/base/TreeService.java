package com.cloud.common.data.base;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cloud.common.data.util.ObjUtil;
import com.cloud.common.entity.TreeEntity;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author Aijm
 * @Description 封装ServiceImpl
 * @Date 2019/10/11
 */
public class TreeService<M extends BaseMapper<T>, T extends TreeEntity> extends BaseService<M, T> implements IService<T>{

    @Autowired
    protected M baseMapper;

    @Override
    public boolean save(T entity) {
        if (StrUtil.isBlank(entity.getParentIds())) {
            T t = baseMapper.selectById(entity.getParentId());
            entity.setParentIds(t.getParentIds()+t.getId()+",");
        }
        return super.save(entity);
    }

}
