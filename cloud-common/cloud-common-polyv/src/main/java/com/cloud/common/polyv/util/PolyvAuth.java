package com.cloud.common.polyv.util;

import lombok.Data;

import java.io.Serializable;

@Data
public class PolyvAuth implements Serializable {
	private static final long serialVersionUID = 1L;

	private String vid;
	private String t;
	private String code;
	private String callback;
	private String secretkey;

}
