package com.cloud.common.cache.constants;


/**
 * 枚举
 * @author Aijm
 * @since 2019/8/29
 */

public enum CacheScope {


     /**
      * 默认存储 系统级别
      */
     application("application"),
     /**
      * 存储 部门信息
      */
     office("office"),

     //---------------------------------     关于用户系统级别缓存key     --------------------------------//
     /**
      * 表示为用户级别  主要为了清空 user:*:{userId}
      */
     user("user"),
     /**
      * 用户 存储了用户信息 （不包含office 和 角色）  user:user:{userId}
      */
     user_user("user:user"),


     /**
      *  存储用户和菜单之间的关系 user:menu:{userId}
      */
     user_menu("user:menu"),
     /**
      *  存储用户和菜单之间的关系 user:role:{userId}
      */
     user_role("user:role"),



     // ---------------------------------     关于用户系统级别缓存key 结束     --------------------------------//


     // ---------------------------------     关于字典系统级别缓存key 开始     --------------------------------//

     /**
      * 存储字典信息 主要为了清空dict:*:{type_code}
      */
     dict("dict"),


     /**
      * 存储字典list信息 dict:list:{type_code}
      */
     dict_list("dict:list"),


     /**
      * 存储字典tree信息 dict:tree:{type_code}
      */
     dict_tree("dict:tree");
     // ---------------------------------     关于字典系统级别缓存key 结束     --------------------------------//
     /**
      *  获取key
      */
     private String cacheName;

     CacheScope(String cacheName) {
          this.cacheName = cacheName;
     }

     public String getCacheName() {
          return cacheName;
     }

}
