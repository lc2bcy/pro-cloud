package com.cloud.email.api;


import cn.hutool.core.util.StrUtil;
import com.cloud.common.util.base.Result;
import com.cloud.common.util.exception.BaseException;
import com.cloud.email.beans.dto.SendEmailDTO;
import com.cloud.email.service.SendEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * @Author Aijm
 * @Description 发送邮件Controller类
 * @Date 2019/9/11
 */
@RestController
public class SendEmailApiImpl implements ISendEmailApi {

    @Autowired
    private SendEmailService sendEmailService;

    @Override
    public Result sendEmail(@RequestBody @Valid SendEmailDTO sendEmail) {

        if (SendEmailDTO.TEXTMAIL.equals(sendEmail.getEmailType())) {

            sendEmailService.sendTextMail(sendEmail.getSubject(), sendEmail.getContent(),
                    sendEmail.getToAddress());

        } else if (SendEmailDTO.TEXTMAIL_CC.equals(sendEmail.getEmailType())) {

            if (StrUtil.isBlank(sendEmail.getToCCAddress())) {
                throw new BaseException("参数邮件抄送地址{sendEmail.getToCCAddress()}为空,无法发送邮件!");
            }
            String[] split = sendEmail.getToCCAddress().split(",");
            sendEmailService.sendTextMail(sendEmail.getSubject(), sendEmail.getContent(),
                    sendEmail.getToAddress(), split);

        } else if (SendEmailDTO.HTMLMAIL.equals(sendEmail.getEmailType())) {

            sendEmailService.sendHtmlMail(sendEmail.getSubject(), sendEmail.getContent(),
                    sendEmail.getToAddress());

        } else if (SendEmailDTO.HTMLMAIL_CC.equals(sendEmail.getEmailType())) {

            if (StrUtil.isBlank(sendEmail.getToCCAddress())) {
                throw new BaseException("参数邮件抄送地址{sendEmail.getToCCAddress()}为空,无法发送邮件!");
            }
            sendEmailService.sendHtmlMail(sendEmail.getSubject(), sendEmail.getContent(),
                    sendEmail.getToAddress(), sendEmail.getToCCAddress());

        } else if (SendEmailDTO.ATTACHMENTSMAIL.equals(sendEmail.getEmailType())) {

            if (StrUtil.isBlank(sendEmail.getFilePath())) {
                throw new BaseException("参数附件地址{sendEmail.getFilePath()}为空,无法发送邮件!");
            }
            sendEmailService.sendAttachmentsMail(sendEmail.getSubject(), sendEmail.getContent(),
                    sendEmail.getFilePath(), sendEmail.getToAddress());

        } else if (SendEmailDTO.ATTACHMENTSMAIL_CC.equals(sendEmail.getEmailType())) {

            if (StrUtil.isBlank(sendEmail.getToCCAddress())) {
                throw new BaseException("参数邮件抄送地址{sendEmail.getToCCAddress()}为空,无法发送邮件!");
            }
            if (StrUtil.isBlank(sendEmail.getFilePath())) {
                throw new BaseException("参数附件地址{sendEmail.getFilePath()}为空,无法发送邮件!");
            }
            sendEmailService.sendAttachmentsMail(sendEmail.getSubject(), sendEmail.getContent(),
                    sendEmail.getFilePath(), sendEmail.getToAddress(), sendEmail.getToCCAddress());

        } else if (SendEmailDTO.INLINERESOURCEMAIL.equals(sendEmail.getEmailType())) {

            sendEmailService.sendInlineResourceMail(sendEmail.getSubject(), sendEmail.getContent(),
                    sendEmail.getRscPath(), sendEmail.getRscId(), sendEmail.getToAddress());

        } else if (SendEmailDTO.INLINERESOURCEMAIL_CC.equals(sendEmail.getEmailType())) {

            if (StrUtil.isBlank(sendEmail.getToCCAddress())) {
                throw new BaseException("参数邮件抄送地址{sendEmail.getToCCAddress()}为空,无法发送邮件!");
            }

            if (StrUtil.isBlank(sendEmail.getRscPath())) {
                throw new BaseException("参数邮件静态资源路径和文件名{sendEmail.getRscPath()}为空,无法发送邮件!");
            }

            if (StrUtil.isBlank(sendEmail.getRscId())) {
                throw new BaseException("参数邮件静态资源id{sendEmail.getRscId()}为空,无法发送邮件!");
            }
            sendEmailService.sendInlineResourceMail(sendEmail.getSubject(), sendEmail.getContent(),
                    sendEmail.getRscPath(), sendEmail.getRscId(), sendEmail.getToAddress(), sendEmail.getToCCAddress());
        }
        return Result.success("");
    }
}
