package com.cloud.email;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 *   基础邮件模块
 * @author Aijm
 * @since  2019/5/8
 */
@SpringBootApplication
public class CloudEmailApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudEmailApplication.class, args);
    }


}
