package com.cloud.auth.service;


import com.cloud.auth.entity.SysUser;
import com.cloud.common.oauth.security.SecurityUser;
import com.cloud.common.oauth.service.ProUserDetailsService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


/**
 *  实现 ProUserDetailsService
 * @author Aijm
 * @since 2019/8/31
 */
@Slf4j
@Service
public class ProUserDetailsServiceImpl implements ProUserDetailsService {

    @Autowired
    private SysUserService sysUserService;


    @Override
    public UserDetails loadUserByMoblie(String mobile) throws UsernameNotFoundException {
        SysUser user = sysUserService.loginByPhone(mobile);
        if (user == null) {
            log.info("{}手机号找不到注册用户", mobile);
            throw new UsernameNotFoundException("手机号异常");
        }
        return new SecurityUser(user.getLoginName(), user.getPassword(), user.getId(), user.getUserType());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        SysUser user = sysUserService.loginByName(username);
        if (user == null) {
            log.info("{}用户名找不到注册用户", username);
            throw new UsernameNotFoundException("用户名异常");
        }
        return new SecurityUser(user.getLoginName(), user.getPassword(), user.getId(), user.getUserType());
    }
}
