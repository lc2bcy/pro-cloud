package ${package}.${moduleName}.mapper;

import com.cloud.common.data.base.BaseMapper;
import ${package}.${moduleName}.beans.po.${className};

/**
 * ${comments}
 *
 * @author ${author}
 * @date ${datetime}
 */
public interface ${className}Mapper extends BaseMapper<${className}> {

}
