package com.cloud.course.mapper;

import com.cloud.common.data.base.TreeMapper;
import com.cloud.course.beans.po.CourseChapter;

/**
 * 章节课时信息
 *
 * @author Aijm
 * @date 2019-10-12 22:41:22
 */
public interface CourseChapterMapper extends TreeMapper<CourseChapter> {

}
