package com.cloud.course.service.impl;

import com.cloud.common.data.base.BaseService;
import com.cloud.course.beans.po.CourseUserLog;
import com.cloud.course.mapper.CourseUserLogMapper;
import com.cloud.course.service.CourseUserLogService;
import org.springframework.stereotype.Service;

/**
 * 课程用户学习详细信息日志
 *
 * @author Aijm
 * @date 2019-10-13 16:46:28
 */
@Service
public class CourseUserLogServiceImpl extends BaseService<CourseUserLogMapper, CourseUserLog> implements CourseUserLogService {

}
