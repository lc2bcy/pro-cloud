package com.cloud.course.mapper;

import com.cloud.common.data.base.BaseMapper;
import com.cloud.course.beans.po.Course;

/**
 * 课程信息
 *
 * @author Aijm
 * @date 2019-10-10 22:02:15
 */
public interface CourseMapper extends BaseMapper<Course> {

}
