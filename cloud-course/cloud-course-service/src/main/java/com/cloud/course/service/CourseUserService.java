package com.cloud.course.service;

import com.cloud.common.data.base.IService;
import com.cloud.course.beans.po.CourseUser;

/**
 * 课程用户关联表
 *
 * @author Aijm
 * @date 2019-10-13 16:40:58
 */
public interface CourseUserService extends IService<CourseUser> {

}
