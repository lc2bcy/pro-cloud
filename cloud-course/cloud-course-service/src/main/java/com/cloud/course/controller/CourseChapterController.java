package com.cloud.course.controller;

import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.course.beans.po.CourseChapter;
import com.cloud.course.service.CourseChapterService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.cloud.common.data.util.ObjUtil;

import javax.validation.Valid;

/**
 * 章节课时信息
 *
 * @author Aijm
 * @date 2019-10-12 22:41:22
 */
@RestController
@RequestMapping("/chapter" )
@Api(value = "chapter", tags = "章节课时管理")
public class CourseChapterController {

    @Autowired
    private CourseChapterService courseChapterService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param courseChapter 章节课时信息
     * @return
     */
    @GetMapping("/page")
    @PreAuthorize("@pms.hasPermission('course_coursechapter_view')")
    public Result getCourseChapterPage(Page page, CourseChapter courseChapter) {
        return Result.success(courseChapterService.page(page, Wrappers.query(courseChapter)));
    }


    /**
     * 通过id查询章节课时信息
     * @param id id
     * @return Result
     */
    @GetMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('course_coursechapter_view')")
    public Result getById(@PathVariable("id") Long id) {
        return Result.success(courseChapterService.getById(id));
    }

    /**
     * 新增章节课时信息
     * @param courseChapter 章节课时信息
     * @return Result
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('course_coursechapter_add')")
    public Result save(@RequestBody @Valid CourseChapter courseChapter) {
        return Result.success(courseChapterService.save(courseChapter));
    }

    /**
     * 修改章节课时信息
     * @param courseChapter 章节课时信息
     * @return Result
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('course_coursechapter_edit')")
    public Result updateById(@RequestBody @Valid CourseChapter courseChapter) {
        return Result.success(courseChapterService.updateById(courseChapter));
    }

    /**
     * 通过id删除章节课时信息
     * @param id id
     * @return Result
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('course_coursechapter_del')")
    public Result removeById(@PathVariable Long id) {
        return Result.success(courseChapterService.removeById(id));
    }

}
