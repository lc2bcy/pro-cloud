package com.cloud.course.service.impl;

import com.cloud.common.data.base.BaseService;
import com.cloud.course.beans.po.CourseUser;
import com.cloud.course.mapper.CourseUserMapper;
import com.cloud.course.service.CourseUserService;
import org.springframework.stereotype.Service;

/**
 * 课程用户关联表
 *
 * @author Aijm
 * @date 2019-10-13 16:40:58
 */
@Service
public class CourseUserServiceImpl extends BaseService<CourseUserMapper, CourseUser> implements CourseUserService {

}
