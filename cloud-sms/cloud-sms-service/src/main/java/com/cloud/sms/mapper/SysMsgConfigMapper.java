package com.cloud.sms.mapper;

import com.cloud.common.data.base.BaseMapper;
import com.cloud.sms.beans.po.SysMsgConfig;

/**
 * 短信配置信息
 *
 * @author Aijm
 * @date 2019-09-10 13:40:14
 */
public interface SysMsgConfigMapper extends BaseMapper<SysMsgConfig> {

}
