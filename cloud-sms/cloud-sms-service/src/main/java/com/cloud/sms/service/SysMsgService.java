package com.cloud.sms.service;

import com.cloud.common.data.base.IService;
import com.cloud.sms.beans.po.SysMsg;

/**
 * 短信发送消息日志
 *
 * @author Aijm
 * @date 2019-09-10 13:45:16
 */
public interface SysMsgService extends IService<SysMsg> {

}
